package com.mycompany.user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired private UserRepository repo;

    public List<User> listAll() {
        return (List<User>)repo.findAll();
    }

    public void save(User user) {
        repo.save(user);
    }

    public User get(Integer id) throws UserNotFoundException {
        Optional<User> userOptional = repo.findById(id);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            return user;
        }else {
            throw new UserNotFoundException("Could not find any users with this ID: " + id);
        }
    }

    public void delete(Integer id) throws UserNotFoundException {
        Optional<User> userOptional = repo.findById(id);
        if(userOptional.isPresent()){
            repo.deleteById(id);
        }else {
            throw new UserNotFoundException("Could not find any users with ID: " + id);
        }
    }

}
